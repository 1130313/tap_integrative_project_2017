package schemaValidationUtils

import scala.util.Try
import javax.xml.validation.SchemaFactory
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import java.io.File

object XMLValidator {
  
  def validateXMLWithSchema(xmlFilename: String, xsdFilename: String): Boolean = {

    Try({
      val sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      val s = sf.newSchema(new File(System.getProperty("user.dir") + "/ip_2017/" + xsdFilename))
      val v = s.newValidator()
      v.validate(new StreamSource(new File(System.getProperty("user.dir") + "/ip_2017/" + xmlFilename)))
      true
    }).getOrElse(false)
  }
}