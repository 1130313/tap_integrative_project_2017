package model

import scala.xml.Elem

case class Human(val id:String,val name:String,lh:List[String]) {
  def toXML():Elem =
  {
    			<Human name={name} />
  }
}