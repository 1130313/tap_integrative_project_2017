package model

import scala.xml.Elem

case class TaskSchedule(startTime: Int,
                        endTime: Int,
                        ord: Order,
                        prdnumber: Int,
                        taskid: String,
                        prl: List[PhysicalResource],
                        hrl: List[Human]) {
  def toXML(): Elem = {
    <TaskSchedule order={ord.id} productNumber={prdnumber.toString()} task={taskid} start={startTime.toString()} end={endTime.toString()}>
    	<PhysicalResources>
        { prl.map(pr => pr.toXML) }
    	</PhysicalResources>
    	<HumanResources>
         { hrl.map(hr => hr.toXML) }
     	</HumanResources>
    </TaskSchedule>
  }

}