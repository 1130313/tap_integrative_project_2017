package model

import scala.xml.Elem

case class PhysicalResource(val id:String, val tp:String) {

  
  def toXML():Elem =
  {
    <Physical id={id}/>
  }
}