package algorithm

import dataReader.ModelData
import model.Task
import model.Order
import model.TaskSchedule
import model.Product
import model.PhysicalResource
import model.Human
import dataWriter.ModelExportData

object ProductionScheduleAlgorithm {

  def getProductionSchedule(algorithName: String, md: ModelData): ModelExportData = {

    val opt = ProductionScheduleAlgorithm(algorithName)
    opt match {
      case None            => ModelExportData(Nil)
      case Some(algorithm) => algorithm(md)
    }
  }

  def apply(filename: String): Option[(ModelData) => ModelExportData] =
    filename match {
      case f if f.equals("ProductionScheduleSimpleAlgorithm") => Some(simpleAlgorithm)
      case f => None
    }

  def verifyEnoughResources(orders: List[Order],
                               products: List[Product],
                               tasks: List[Task],
                               physicalResources: List[PhysicalResource],
                               humans: List[Human]): Boolean = {
    //ver as tasks necessárias para cada order
    val tskNeededForOrder = orders.map(o => products.find(p => p.id.equals(o.pr)).get.tl)
                                  .flatMap(t=>t).toSet[String].toList
      .map(tID => tasks.find(t => t.id.equals(tID)).get)
    //para cada task transformar num map que cada key é um tipo de physical resource
    //e cada value é a quantidade desse tipo necessário para esta tarefa
    val grtl = tskNeededForOrder.map(t => t.prl.groupBy(tp => tp).map { case (k, v) => (k, v.size) })
    //para cada PR transformar num map que cada key 
    //é um tipo de physical resource e o value é a quantidade existente
    val prm = physicalResources.map(pr => pr.tp).groupBy(t => t).map { case (k, v) => (k, v.size) }
    //o mesmo para cada human resource
    val hrm = humans.flatMap(hr => hr.lh).groupBy(t => t).map { case (k, v) => (k, v.size) }
    //verificar que para cada task existe a quantidade necessária de physical resources
    //e human para cada tipo 
    grtl.forall(task => task.forall { case (k, v) => ((prm get k).getOrElse(0) >= v) &&
                                                     ((hrm get k).getOrElse(0) >= v) })
  }

  def availableHumans(prType: String, humans: Map[String, List[Human]], occupied: List[Human]): List[Human] =
    {
      humans.get(prType).get.filter(hr => (!occupied.contains(hr)))
    }

  def allocateHumanResources(taskTypes: List[String],
                             hrGroupedByType: Map[String, List[Human]],
                             occupiedHR: List[Human]): List[Human] =

    taskTypes match {
      case Nil => Nil
      case t1 :: Nil =>
        val ahr = availableHumans(t1, hrGroupedByType, occupiedHR)
        ahr match {
          case Nil      => Nil
          case hr1 :: _ => hr1 :: Nil
        }
      case t1 :: tail =>
        val ahr = availableHumans(t1, hrGroupedByType, occupiedHR)
        ahr match {
          case Nil => Nil
          case ahr =>
            //tenta para todos os human resources disponiveis
            //encontrar o primeiro que ao ser ocupado mesmo assim é possível alocar mais human resources
            //caso retorne h::Nil significa que não encontrou mais humans
            //case retorne dois elementos, significa que encontrou, então retorna sucesso
            ahr.toStream.map(h => h :: allocateHumanResources(tail, hrGroupedByType, h :: occupiedHR))
              .find {
                case h :: Nil                          => false
                case h :: h1 if (h1.size == tail.size) => true
              }.getOrElse(Nil)
        }
    }
  def availablePhysicalResources(prType: String, physicalResources: Map[String, List[PhysicalResource]], occupied: List[PhysicalResource]): List[PhysicalResource] =
    {
      physicalResources.get(prType).get.filter(pr => (!occupied.contains(pr)))
    }
  def allocatePhysicalResources(taskTypes: List[String],
                                prGroupedByType: Map[String, List[PhysicalResource]],
                                occupiedPR: List[PhysicalResource]): List[PhysicalResource] =
    taskTypes match {
      case Nil => Nil
      case t1 :: tail =>
        val apr = availablePhysicalResources(t1, prGroupedByType, occupiedPR)
        val allocatedPR = apr.head
        allocatedPR :: allocatePhysicalResources(tail, prGroupedByType, allocatedPR :: occupiedPR)
    }
  def simpleAlgorithm(modelData: ModelData): ModelExportData = {

    val orders = modelData.ol
    val products = modelData.pl
    val physicalResources = modelData.prl
    val tasks = modelData.tl
    val humans = modelData.hrl

    def processTask(initialTime: Int, tsk: Task, productNumber: Int, order: Order): TaskSchedule =
      {
        val endTime = initialTime + tsk.time

        //lista de physical resources agrupados por tipo
        val prGroupedByType = physicalResources.groupBy(pr => pr.tp)
        //list de human resources agrupados por tipo
        val hrGroupedByType = humans
          .flatMap(hr => hr.lh).toSet[String]
          .map(prType => prType -> humans.filter(pr => pr.lh.contains(prType)))
          .toMap[String, List[Human]]
        //.map { case (k, v) => (k, v.sortWith(_.lh.size < _.lh.size)) }
        //alocar recursos físicos
        val prl = allocatePhysicalResources(tsk.prl, prGroupedByType, Nil)
        //atribuir tarefas a humanos
        val hrl = allocateHumanResources(tsk.prl, hrGroupedByType, Nil)

        TaskSchedule(initialTime, endTime, order, productNumber, tsk.id, prl, hrl)
      }
    def processProductTasks(initialTime: Int, tsks: List[Task], productNumber: Int, order: Order): List[TaskSchedule] = tsks match {
      case Nil => Nil
      case tsk :: tail =>
        val tskSch = processTask(initialTime, tsk, productNumber, order)
        tskSch :: processProductTasks(tskSch.endTime, tail, productNumber, order)
    }
    def processProduct(initialTime: Int, prd: Product, prdRange: List[Int], order: Order): List[TaskSchedule] =
      {
        val prdtsksStrs = prd.tl
        val prdtsks = prdtsksStrs.map(tskid => tasks.find(t => t.id.equals(tskid)).get)
        prdRange match {
          case Nil => Nil
          case prdNr2 :: t =>
            val taskScheduleList = processProductTasks(initialTime, prdtsks, prdNr2, order)
            taskScheduleList ::: processProduct(taskScheduleList.last.endTime, prd, t, order)
        }
      }
    def processOrders(initialTime: Int, orders: List[Order]): List[TaskSchedule] = orders match {
      case Nil => Nil
      case o1 :: tail =>
        val prd = products.find(p => p.id.equals(o1.pr)).get
        val prdRange = (1 to o1.qtd).toList
        val tkscheduleList = processProduct(initialTime, prd, prdRange, o1)
        tkscheduleList ::: processOrders(tkscheduleList.last.endTime, tail)
    }

    val isPossible = verifyEnoughResources(orders, products, tasks, physicalResources, humans)
    isPossible match {
      case true =>
        val list = processOrders(0, orders)
        //val totalTskSch = orders.map(o => products.find(p => p.id.equals(o.pr)).get.tl.size * o.qtd).reduceLeft(_+_)
        val sucess = list.forall(tskSch=>tskSch.prl.size==tskSch.hrl.size)
        sucess match
        {
            case true => ModelExportData(list)
            case false => ModelExportData(Nil)
        } 
      case false =>
        ModelExportData(Nil)
    }
  }

}