package dataWriter

import scala.xml.XML
import scala.xml.NamespaceBinding
import scala.xml.TopScope
import scala.xml.PrefixedAttribute
import scala.xml.transform.RuleTransformer
import scala.xml.UnprefixedAttribute
import scala.xml.Node
import scala.xml.transform.RewriteRule
import scala.xml.Elem
import schemaValidationUtils.XMLValidator


object DataWriter {
  
  def writeScheduleDataToFile(filename: String, m: ModelExportData) = {
    val opt = DataWriter(filename)
    opt match {
      case None         => Unit
      case Some(writer) => writer(filename, m)
    }
  }

  def apply(filename: String): Option[(String, ModelExportData) => Unit] =
    filename match {
      case f if f.endsWith(".xml") => Some(writeXML)
      case f                       => None
    }

  
  
  def writeXML(filename: String, m: ModelExportData): Unit = {
    val filePath = System.getProperty("user.dir") + "/ip_2017/" + filename
    val xsi = new NamespaceBinding("xsi", "http://www.w3.org/2001/XMLSchema-instance", TopScope)
    val exportDataInXML = <Schedule xmlns={"http://www.dei.isep.ipp.pt/ip_2017"} xmlns:xsi={"http://www.w3.org/2001/XMLSchema-instance"} xsi:schemaLocation={ "http://www.dei.isep.ipp.pt/ip_2017 ip_2017_out.xsd" }> { m.tsl.map(ts => ts.toXML) }</Schedule>
    XML.save(filePath, exportDataInXML, "UTF-8", true, null)
    
    // Validate output file
    XMLValidator.validateXMLWithSchema(filename, "ip_2017_out.xsd") match {
      case false  => println("Output file validation error!")
      case true   => println("Data written successfully!") 
    }
  }

}