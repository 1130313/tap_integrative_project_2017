package main

import dataReader.DataReader
import dataWriter.DataWriter
import algorithm.ProductionScheduleAlgorithm

object Main {
  
  def main(args: Array[String]): Unit = {
    
    val data = DataReader.getProductionDataFromFile("ip_2017_in.xml")
    val exportData = ProductionScheduleAlgorithm.getProductionSchedule("ProductionScheduleSimpleAlgorithm",data)
    val writeData = DataWriter.writeScheduleDataToFile("ip_2017_out.xml", exportData)
  }
}