package tests

import algorithm.ProductionScheduleAlgorithm
import model.Order
import model.Product
import model.Task
import model.PhysicalResource
import model.Human
import org.scalatest.FunSuite

class VerifyEnoughResourcesTest extends FunSuite {

  def verifySchedule(physicalResources:List[PhysicalResource],
                     humans:List[Human]):Boolean = {
    val orders = List(Order("o1ID", "p1ID", 3))
    val products = List(Product("p1ID", "produto 1", List("t1ID", "t2ID", "t3ID")))
    val tasks = List(Task("t1ID", 100, List("type1", "type2", "type3")),
                     Task("t2ID", 150, List("type1")), 
                     Task("t3ID", 50, List("type2")))
    ProductionScheduleAlgorithm.verifyEnoughResources(orders, products, tasks, physicalResources, humans)                                 
  }

  test("Verify Impossible Schedule") {

    val physicalResources = List(PhysicalResource("pr1", "type1"), PhysicalResource("pr2", "type2"), PhysicalResource("pr3", "type3"), PhysicalResource("pr4", "type1"), PhysicalResource("pr5", "type2"))
    val humans = List(Human("hr1", "Paulo", List("type1")),
      Human("hr2", "Paulo", List("type2")),
      Human("hr3", "Paulo", List("type3")))
    
    assert(verifySchedule(physicalResources,humans))
  }
  test("Verify Impossible Schedule Fail Not Enough Humans to Handle PRTYPE") {
    
    val physicalResources = List(PhysicalResource("pr1", "type1"), PhysicalResource("pr2", "type2"), PhysicalResource("pr3", "type3"), PhysicalResource("pr4", "type1"), PhysicalResource("pr5", "type2"))
    val humans = List(Human("hr1", "Paulo", List("type1")),
      Human("hr2", "Paulo", List("type1")),
      Human("hr3", "Paulo", List("type1")))
    assert(!verifySchedule(physicalResources,humans))
  }
  test("Verify Impossible Schedule Fail Not Enough PR") {
    val physicalResources = List(PhysicalResource("pr1", "type1"), PhysicalResource("pr2", "type1"))
    //Não há human suficientes
    val humans = List(Human("hr1", "Paulo", List("type1")),
      Human("hr2", "Paulo", List("type2")),
      Human("hr3", "Paulo", List("type3")))
    assert(!verifySchedule(physicalResources,humans))
  }

}