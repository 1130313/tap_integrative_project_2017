package tests

import org.scalatest.FunSuite
import model._
import algorithm.ProductionScheduleAlgorithm

class AllocateHumanResourcesTest extends FunSuite {

  test("Allocate Human Resources") {

    //val orders = List(Order("o1ID", "p1ID", 3))
    //val products = List(Product("p1ID", "produto 1", List("t1ID", "t2ID", "t3ID")))
    val task = Task("t1ID", 100, List("type1", "type2", "type3"))
    
    val humans = List(Human("hr1", "Paulo", List("type1")),
      Human("hr2", "Paulo", List("type2")),
      Human("hr3", "Paulo", List("type3")))

    val hrGroupedByType = humans
      .flatMap(hr => hr.lh).toSet[String]
      .map(prType => prType -> humans.filter(pr => pr.lh.contains(prType)))
      .toMap[String, List[Human]]

    val humanResourcesAllocated = ProductionScheduleAlgorithm.allocateHumanResources(task.prl, hrGroupedByType, Nil)

    assert(humanResourcesAllocated.size == task.prl.size)

  }
  test("Allocate Human Resources Changed Order") {

    //val orders = List(Order("o1ID", "p1ID", 3))
    //val products = List(Product("p1ID", "produto 1", List("t1ID", "t2ID", "t3ID")))
    val task = Task("t1ID", 100, List("type1", "type2", "type3"))
    val physicalResources = List(PhysicalResource("pr1", "type1"), PhysicalResource("pr2", "type2"), PhysicalResource("pr3", "type3"), PhysicalResource("pr4", "type1"), PhysicalResource("pr5", "type2"))
    val humans = List(
      Human("hr3", "Paulo", List("type3","type2","type1")),
      Human("hr2", "Paulo", List("type2")),
      Human("hr1", "Paulo", List("type1")))

    val hrGroupedByType = humans
      .flatMap(hr => hr.lh).toSet[String]
      .map(prType => prType -> humans.filter(pr => pr.lh.contains(prType)))
      .toMap[String, List[Human]]

    val humanResourcesAllocated = ProductionScheduleAlgorithm.allocateHumanResources(task.prl, hrGroupedByType, Nil)

    assert(humanResourcesAllocated.size == task.prl.size)
  }
  
    test("Allocate Human Not Enough Resources") {

    //val orders = List(Order("o1ID", "p1ID", 3))
    //val products = List(Product("p1ID", "produto 1", List("t1ID", "t2ID", "t3ID")))
    val task = Task("t1ID", 100, List("type1", "type2", "type3"))
    val physicalResources = List(PhysicalResource("pr1", "type1"), PhysicalResource("pr2", "type2"), PhysicalResource("pr3", "type3"), PhysicalResource("pr4", "type1"), PhysicalResource("pr5", "type2"))
    val humans = List(
      Human("hr3", "Paulo", List("type3","type2","type1")),
      Human("hr2", "Paulo", List("type1")),
      Human("hr1", "Paulo", List("type1")))

    val hrGroupedByType = humans
      .flatMap(hr => hr.lh).toSet[String]
      .map(prType => prType -> humans.filter(pr => pr.lh.contains(prType)))
      .toMap[String, List[Human]]

    val humanResourcesAllocated = ProductionScheduleAlgorithm.allocateHumanResources(task.prl, hrGroupedByType, Nil)
    //not possible to allocate
    assert(humanResourcesAllocated.size == 0)
  }
  

}