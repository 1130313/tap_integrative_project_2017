package tests

import org.scalatest.FunSuite
import model._
import algorithm.ProductionScheduleAlgorithm

class AllocatePhysicalResourcesTest extends FunSuite {

  test("Allocate Physical Resources") {
    val task = Task("t1ID", 100, List("type1", "type2", "type3"))
    val physicalResources = List(PhysicalResource("pr1", "type1"), PhysicalResource("pr2", "type2"), PhysicalResource("pr3", "type3"), PhysicalResource("pr4", "type1"), PhysicalResource("pr5", "type2"))
    val prGroupedByType = physicalResources.groupBy(pr => pr.tp)
    
    assert(ProductionScheduleAlgorithm.allocatePhysicalResources(task.prl, prGroupedByType, Nil).size == task.prl.size)
  }
}