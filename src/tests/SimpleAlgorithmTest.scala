package tests

import org.scalatest.FunSuite
import dataReader.ModelData
import dataWriter.ModelExportData
import model._
import algorithm.ProductionScheduleAlgorithm

class SimpleAlgorithmTest extends FunSuite {

  test("Simple Algorithm Test") {
    val orders = List(Order("OD1", "PRD1", 2))
    val prds = List(Product("PRD1", "Product 1", List("TSK1", "TSK2")))
    val tsks = List(Task("TSK1", 100, List("T1", "T2")), Task("TSK2", 50, List("T1")))
    val prs = List(PhysicalResource("PR1", "T1"), PhysicalResource("PR2", "T2"))
    val hs = List(Human("HMN1", "Paulo", List("T1", "T2")), Human("HMN2", "Vitor", List("T2")))

    val model = ModelData(orders, prds, tsks, prs, hs)

    val result = ProductionScheduleAlgorithm.simpleAlgorithm(model)

    val tsksch1 = TaskSchedule(0, 100, Order("OD1", "PRD1", 2), 1, "TSK1",
      List(PhysicalResource("PR1", "T1"), PhysicalResource("PR2", "T2")),
      List(Human("HMN1", "Paulo", List("T1", "T2")), Human("HMN2", "Vitor", List("T2"))))

    val tsksch2 = TaskSchedule(100, 150, Order("OD1", "PRD1", 2), 1, "TSK2",
      List(PhysicalResource("PR1", "T1")),
      List(Human("HMN1", "Paulo", List("T1", "T2"))))

    val tsksch3 = TaskSchedule(150, 250, Order("OD1", "PRD1", 2), 2, "TSK1",
      List(PhysicalResource("PR1", "T1"), PhysicalResource("PR2", "T2")),
      List(Human("HMN1", "Paulo", List("T1", "T2")), Human("HMN2", "Vitor", List("T2"))))

    val tsksch4 = TaskSchedule(250, 300, Order("OD1", "PRD1", 2), 2, "TSK2",
      List(PhysicalResource("PR1", "T1")),
      List(Human("HMN1", "Paulo", List("T1", "T2"))))

    assert(ModelExportData(List(tsksch1, tsksch2, tsksch3, tsksch4)) == result)

  }

  test("Simple Algorithm Test Not Enough Human") {

    val orders = List(Order("OD1", "PRD1", 2))
    val prds = List(Product("PRD1", "Product 1", List("TSK1", "TSK2")))
    val tsks = List(Task("TSK1", 100, List("T1", "T2","T3")), Task("TSK2", 50, List("T1")))
    val prs = List(PhysicalResource("PR1", "T1"), PhysicalResource("PR2", "T2"),PhysicalResource("PR3", "T3"))
    val hs = List(Human("HMN1", "Paulo", List("T1", "T2","T3")), Human("HMN2", "Vitor", List("T2")))
 
    val model = ModelData(orders, prds, tsks, prs, hs)

    val result = ProductionScheduleAlgorithm.simpleAlgorithm(model)

    assert(ModelExportData(Nil) == result)

  }
  
  test("Simple Algorithm Test Not Enough PR")
  {
    val orders = List(Order("OD1", "PRD1", 2))
    val prds = List(Product("PRD1", "Product 1", List("TSK1", "TSK2")))
    val tsks = List(Task("TSK1", 100, List("T1", "T2","T3")), Task("TSK2", 50, List("T1")))
    val prs = List(PhysicalResource("PR1", "T1"), PhysicalResource("PR2", "T2"))
    val hs = List(Human("HMN1", "Paulo", List("T1", "T2","T3")),Human("HMN3", "Luís", List("T1", "T2","T3")), Human("HMN2", "Vitor", List("T2")))

    val model = ModelData(orders, prds, tsks, prs, hs)

    val result = ProductionScheduleAlgorithm.simpleAlgorithm(model)

    assert(ModelExportData(Nil) == result)
    
    
  }

}