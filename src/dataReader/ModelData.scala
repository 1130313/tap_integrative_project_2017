package dataReader

import model._

case class ModelData (val ol: List[Order], 
    val pl: List[Product], 
    val tl: List[Task], 
    val prl: List[PhysicalResource],
    val hrl: List[Human]) {
  
}