package dataReader

import scala.xml.XML
import model._
import schemaValidationUtils.XMLValidator

object DataReader {
  
  def getProductionDataFromFile(filename: String): ModelData = {
    
    val opt = DataReader(filename)
    opt match {
      case None => ModelData.apply(List[Order](), List[Product](), List[Task](), List[PhysicalResource](), List[Human]())
      case Some(parser) => parser(filename)
      
    }
  }
  
  def apply(filename: String): Option[(String) => ModelData] =
    filename match {
      case f if f.endsWith(".xml") => Some(loadXMLWithSchema)
      case f                       => None
  }
  
  def loadXMLWithSchema(filename: String) = {
    
    XMLValidator.validateXMLWithSchema(filename, "ip_2017_in.xsd") match {
      case true   => println("Input file loaded correctly!");
                     val xml = XML.loadFile(System.getProperty("user.dir") + "/ip_2017/" + filename)
                     parseXml(xml)
      case false  => println("Input file validation error!")
                     ModelData(Nil, Nil, Nil, Nil, Nil)
    }
    
  }
  
  def parseXml(xmlFile: scala.xml.Elem): ModelData = {
    
    // Create physical resources list
    val pr = xmlFile \ "PhysicalResources"  \ "Physical"
    val prl = pr.map(node => PhysicalResource(node.attributes("id").toString(), node.attributes("type").toString())).toList
    
    // Create tasks list
    val tasks = xmlFile \ "Tasks" \ "Task"
    val tl = tasks.map(node => Task(node.attributes("id").toString(), node.attributes("time").toString().toInt, (node \ "PhysicalResource").map(childNode => childNode.attributes("prstype").toString()).toList)).toList
    
    // Create human resources list
    val humans = xmlFile \ "HumanResources" \ "Human"
    val hrl = humans.map(node => Human(node.attributes("id").toString(), node.attributes("name").toString(), (node \ "Handles").map(childNode => childNode.attributes("type").toString()).toList)).toList
    
    // Create products list
    val products = xmlFile \ "Products" \ "Product" //> products  : scala.xml.NodeSeq = NodeSeq(<Product name="Product 1" id="PRD_1"
    val pl = products.map(node => Product(node.attributes("id").toString(), node.attributes("name").toString(), (node \ "Process").map(childNode => childNode.attributes("tskref").toString()).toList)).toList
    
    // Create orders list
    val orders = xmlFile \ "Orders" \ "Order"
    val ol = orders.map(node => Order(node.attributes("id").toString(), node.attributes("prdref").toString(), node.attributes("quantity").toString().toInt)).toList
    
    ModelData(ol, pl, tl, prl, hrl)
  }
    
}